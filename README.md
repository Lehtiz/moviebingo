# Movie bingo app

Creates a printable bingo card for a movie based on movies main genre.
_Currently only has basic functionality._

### TODO ideas:
* Form to submit new bingo data
* Difficulty:
  * Random free cells to make game easier if wanted
  * Rarity in json data and weights
* reroll button next to selector
