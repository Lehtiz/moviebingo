import React, { Component } from 'react';
import './App.css';
import Header from './components/Header'
import Help from './components/Help'
import Bingo from './components/Bingo'
import Footer from './components/Footer'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <Help />
        <Bingo />
        <Footer />
      </div>
    );
  }
}



export default App;
