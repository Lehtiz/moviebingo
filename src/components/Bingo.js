import React, { Component } from 'react';
import './Bingo.css';
import RenderTable from './RenderTable'
const data = require('../data/bingodata.json');


class Bingo extends Component {
  render() {
    return (
      <div>
        <section className="App-bingotable" id="react-print">
          <RenderTable data={data}/>
        </section>
      </div>
    );
  }
}
export default Bingo;
