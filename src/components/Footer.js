import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <div className="App-footer" id="react-no-print">
        <p>Hello from Footer component</p>
      </div>
    );
  }
}

export default Footer;
