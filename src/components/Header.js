import React, { Component } from 'react';

class Header extends Component {
  render() {
    return (
      <div className="App-header" id="react-no-print">
        <p>Moviebingo app</p>
      </div>
    );
  }
}

export default Header;
