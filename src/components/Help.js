import React, { Component } from 'react';

class Help extends Component {
  render() {
    return (
      <div className="App-help" id="react-no-print">
        <p>Hello from Help component!</p>
        <p>There are namy paragraphs here.</p>
      </div>
    );
  }
}

export default Help;
