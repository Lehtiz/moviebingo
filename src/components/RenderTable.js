import React, { Component } from 'react';
import keyIndex from 'react-key-index';

class RenderTable extends Component {
  constructor(props) {
    super(props);
    //console.log("RenderTable constructor props: ", props)

    //selector needs
    this.handleChange = this.handleChange.bind(this);
    this.state = {value: 'Select genre'};
    var myGenreSet = new Set();
    var validGenreSet = new Set();
    myGenreSet.add("Select Genre")
    this.props.data.forEach((data) => {
      myGenreSet.add(data.category)
      validGenreSet.add(data.category)
    })
    //convert to Array
    let filteredSelectorData = Array.from(myGenreSet);
    let filteredValidSelectorData = Array.from(myGenreSet);
    //bind needed
    this.filteredSelectorData = filteredSelectorData
    this.filteredValidSelectorData = filteredValidSelectorData

  }
  //eventhandler for selector change
  handleChange(event) {
    this.setState({value: event.target.value});
    //console.log("RenderSelector change: ",event.target.value)
  }
  DrawOptions = ({props}) =>{
    //console.log("DrawOptions props: ",props)
    return(<option value={props}>{props}</option>)
  }
  DrawSelect = ({props}) =>{
    //console.log("DrawSelect props: ",props)
    return(
      <select value={this.state.value} onChange={this.handleChange}>
        {
          props.map((d,i) =>{return(<this.DrawOptions key={i.toString()} props={d} />)})
        }
      </select>
    )
  }

  //Fisher–Yates Shuffle
  shuffle = (array) => {
    var m = array.length, t, i;

    // While there remain elements to shuffle…
    while (m) {

      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);

      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }
    return array;
}

  render() {
    //console.log("RenderTable render data: ",this.props.data)
    //filter data to only show selected genre
    var filteredData = this.props.data.filter(el => el.category === this.state.value);
    //console.log("RenderTable render filteredData: ",filteredData)
    //new array for stripped content data
    var newarr = []
    filteredData.map((d) =>{newarr.push(d.content)}) //only take content into new array
    newarr = this.shuffle(newarr) //shuffle for more fun

    return (
      <section className="bingotable">
        <label id="react-no-print">
          Pick genre:
          <this.DrawSelect  props={this.filteredSelectorData} />
        </label>
        <table>
          <CreateTable data={newarr} />
        </table>
      </section>
    );
  }
}

class CreateTable extends React.Component {
  render(){
    //console.log("CreateTable: this.props.data ", this.props.data)
    const rowdata1 = [];
    const rowdata2 = [];
    const rowdata3 = [];
    const rowdata4 = [];
    const rowdata5 = [];


    this.props.data.forEach((data, index) => {
    //console.log(data," data and index ", index)

      if (index >= 0 && index < 5) {
        rowdata1.push(<CreateCell data={data} id={index} />)
      }
      if (index >= 5 && index < 10) {
        rowdata2.push(<CreateCell data={data} id={index} />)
      }
      if (index >= 10 && index < 15) {
        rowdata3.push(<CreateCell data={data} id={index} />)
      }
      if (index >= 15 && index < 20) {
        rowdata4.push(<CreateCell data={data} id={index} />)
      }
      if (index >= 20 && index < 25) {
        rowdata5.push(<CreateCell data={data} id={index} />)
      }
    });

    return (
      <tbody>
        <CreateRow data={rowdata1} key={rowdata1.index} />
        <CreateRow data={rowdata2} key={rowdata2.index} />
        <CreateRow data={rowdata3} key={rowdata3.index} />
        <CreateRow data={rowdata4} key={rowdata4.index} />
        <CreateRow data={rowdata5} key={rowdata5.index} />
      </tbody>
    )
  }
}

class CreateRow extends React.Component {
  render(){
    const data = this.props.data;
    const id = this.props.key;
    //console.log("CreateRow constructor props: ", data, id)
    return (
      <tr key={id}>{data}</tr>
    );
  }
}

class CreateCell extends React.Component {
  render(){
    const data = this.props.data;
    const id = this.props.key;
    //console.log("CreateCell constructor props: ", this.props, data)
    return (
      <td key={id}>{data}</td>
    );
  }
}

export default RenderTable;
